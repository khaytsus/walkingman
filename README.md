# walkingman
Walking Man hack that a bunch of little men walk around on the screen.  Amiga source code, plus XPenguins theme.

In the 1980's this cute hack came out on the Amiga which showed a bunch of little guys walking around randomly on the screen, falling off windows, shining their flashlights in the darkness.  This code is likely nearly lost to time, so it's here.  I do not have compilation instructions, although I imagine it's not terribly difficult, but it's also lost in time.  I do have a binary that can be used, if you put these files on an Amiga, in UAE etc, you should be able to use it.

Years ago I converted this into a XPenguins theme and that is also included.  Drop it into your ~/.xpenguins/themes directory  It's not exactly like the original but it's pretty close.
